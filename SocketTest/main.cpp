#include <iostream>
#include <thread>
#include <string>

#include "TcpSocket.hpp"

using namespace SocketTest;

void Countdown(int seconds)
{
  for(int i = seconds; i > 0; --i)
  {
    std::this_thread::sleep_for(std::chrono::seconds(1));
    
    std::cout << i << '\n';
  }
  
  std::this_thread::sleep_for(std::chrono::seconds(1));
}

int main(int argc, const char * argv[])
{
  TcpSocket tcpSock;
  
  if(!tcpSock.Connect("173.194.70.94", 80))
  {
    std::cout << "Whoa! Couldn't connect!\n";
    return 0;
  }
  
  Countdown(2);
  std::cout << "Should be connected\n";
  
  const std::string request = "GET /\n\rHost: www.google.com\n\r\n\r";
  tcpSock.Send((void*)request.c_str(), request.size());
  Countdown(2);
  
  while(tcpSock.Available())
  {
    /* Or I could just use tcpSock.Available() to get the recieved data length. */
    
    unsigned char *data = new unsigned char[513];
    tcpSock.Recv(data, 512);
    data[512] = 0;
    
    std::cout << data;
    
    delete[] data;
    
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
  }
  
  std::cout << "Ok\n";
}

