#pragma once

#include "Socket.hpp"

#include <string>

namespace SocketTest
{
  class TcpSocket: public Socket
  {
  public:
    TcpSocket();
    virtual ~TcpSocket();
    
    ssize_t Recv(void *dstBuf, size_t bufSize) override;
    ssize_t Send(const void *srcBuf, size_t bufSize) override;
    
    bool Connect(const std::string &ip, int port);
  };
}