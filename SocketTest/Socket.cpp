#include "Socket.hpp"

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
//#include <netinet/in.h>
//#include <sys/un.h>
//#include <arpa/inet.h>
//#include <netdb.h>

#include <iostream>
#include <cassert>

namespace SocketTest
{
  Socket::Socket()
  : sd(0)
  {}
  
  Socket::~Socket()
  {
    close(sd);
  }
  
  void Socket::SetBlockMode(BlockMode mode)
  {
    int flags = fcntl(sd, F_GETFL, 0);
    
    switch(mode)
    {
      case Blocking: /* Not tested! */
        if(flags & O_NONBLOCK)
          flags ^= O_NONBLOCK;
        
        assert(!(flags & O_NONBLOCK));
        break;
      case NonBlocking:
        flags |= O_NONBLOCK;
        break;
    }
    
    fcntl(sd, F_SETFL, flags);
  }
  
  bool Socket::CanRead() const
  {
    fd_set set;
    
    FD_ZERO(&set);
    FD_SET(sd, &set);
    
    std::cout << "Socket::CanRead() - Calling select\n";
    
    return select(sd + 1, &set, nullptr, nullptr, nullptr) > 0;
  }
  
  bool Socket::CanWrite() const
  {
    fd_set set;
    
    FD_ZERO(&set);
    FD_SET(sd, &set);
    
    std::cout << "Socket::CanWrite() - Calling select\n";
    
    return select(sd + 1, nullptr, &set, nullptr, nullptr) > 0;
  }
  
  int Socket::Available() const
  {
    int count = 0;
    ioctl(sd, FIONREAD, &count);
    return count;
  }
}