#include "TcpSocket.hpp"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdexcept>
#include <iostream>

namespace SocketTest
{
  TcpSocket::TcpSocket()
  : Socket()
  {
    sd = socket(PF_INET, SOCK_STREAM, 0);
    if(sd < 0)
      throw std::runtime_error("TcpSocket::TcpSocket() - socket() failed!");
    
    SetBlockMode(Socket::NonBlocking);
  }
  
  TcpSocket::~TcpSocket() {}
  
  ssize_t TcpSocket::Recv(void *dstBuf, size_t bufSize)
  {
    return recv(sd, dstBuf, bufSize, 0);
  }
  
  ssize_t TcpSocket::Send(const void *srcBuf, size_t bufSize)
  {
    return send(sd, srcBuf, bufSize, 0);
  }
  
  bool TcpSocket::Connect(const std::string &ip, int port)
  {
    struct sockaddr_in addr;
    
    addr.sin_family = AF_INET;
    inet_pton(AF_INET, ip.c_str(), &addr.sin_addr);
    addr.sin_port = htons(port);
    
    int ret = connect(sd, (struct sockaddr*)&addr, sizeof(addr));
    if(ret < 0)
    {
      if(errno == EINPROGRESS)
      {
        ret = 0;
      }
    }
    
    return ret >= 0;
  }
}