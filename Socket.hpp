#pragma once

#include <cstddef>
#include <sys/types.h>

namespace SocketTest
{
  class Socket
  {
  public:
    enum BlockMode
    {
      Blocking,
      NonBlocking
    };
  protected:
    int sd;
  public:
    Socket();
    virtual ~Socket();
    
    virtual ssize_t Recv(void *dstBuf, size_t bufSize) = 0;
    virtual ssize_t Send(const void *srcBuf, size_t bufSize) = 0;
    
    void SetBlockMode(BlockMode mode);
    
    bool CanRead() const;
    bool CanWrite() const;
    int Available() const;
  };
}